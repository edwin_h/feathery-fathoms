import java.util.Random;

/**
 * Created by Fubar on 12/12/13.
 */
public class Chance {
    private static Random r = new Random();

    public static int integer(int min, int max){
        return r.nextInt((max-min)+1)+min;
    }

    public static boolean percent(int perc){
        int n = integer(0, 100);
        if(n < perc){
            return true;
        }else{
            return false;
        }
    }
}
