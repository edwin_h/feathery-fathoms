import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by fubar on 12/11/13.
 */
public class World {
    private final int MAX_TILESIZE = 128;
    private final int MIN_TILESIZE = 8;
    private ArrayList<Tile> tiles;
    private int width;
    private int height;
    private Image background;

    private int tileSize = 16;

    public World(){

    }

    public void loadMapFile(String loc){
        int map_width = 0;
        int map_height = 0;
        String map_background = null;
        Object map_tiles = null;

        try{
            File file = new File(loc);
            String s = new Scanner(file).useDelimiter("//Z").next();
            JSONObject json = (JSONObject) new JSONParser().parse(s);

            for(Object k : json.keySet()){
                String key = (String) k;
                Object value = json.get(k);
                switch (key) {
                    case "width":
                        map_width = Integer.parseInt(value.toString());
                        break;
                    case "height":
                        map_height = Integer.parseInt(value.toString());
                        break;
                    case "background":
                        map_background = value.toString();
                        break;
                    case "tiles":
                        map_tiles = value;
                        break;
                }
            }

            createMap(map_width, map_height, map_background, map_tiles);

        }catch(Exception e){
            e.printStackTrace();
        }
    }



    public void createMap(int w, int h, String bg, Object ts){
        tiles = new ArrayList<>();
        width = w;
        height = h;

        try {
            background = new Image(bg);
        } catch (SlickException e) {
            e.printStackTrace();
        }

        JSONArray tileArray = (JSONArray) ts;
        for(Object t : tileArray){
            JSONObject td = (JSONObject) t;

            int tile_row = Integer.parseInt(td.get("row").toString());
            int tile_col = Integer.parseInt(td.get("col").toString());
            String tile_type = td.get("type").toString();

            Tile tile = new Tile(tile_row, tile_col);
            tile.setType(tile_type);
            tiles.add(tile);
        }

        System.out.println("Created new world with "+tiles.size()+" tiles");
    }

    public int getTileSize() {
        return tileSize;
    }

    public void setTileSize(int ts) {
        if(ts < MIN_TILESIZE){
            this.tileSize = MIN_TILESIZE;
        }else if(ts > MAX_TILESIZE){
            this.tileSize = MAX_TILESIZE;
        }else{
            this.tileSize = ts;
        }
        System.out.println("Tilesize is now "+this.tileSize);
    }

    public Tile getTile(int r, int c){
        for(Tile tile : tiles){
            if(tile.getRow() == r && tile.getCol() == c){
                return tile;
            }
        }
        return null;
    }

    public ArrayList<Tile> getTiles() {
        return tiles;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Image getBackground() {
        return background;
    }
}
