import org.newdawn.slick.*;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by fubar on 12/11/13.
 */
public class Main extends BasicGame{
    private static final String TITLE = "Feathery Fathoms";
    private AppGameContainer appgc;
    private World world;
    private Character player;
    private ArrayList<Snowflake> snowflakes;


    @Override
    public void init(GameContainer gc) throws SlickException {
        world = new World();
        world.loadMapFile("maps/test.json");

        snowflakes = new ArrayList<>();

        player = new Character("Player");
        SpriteSheet playerSheet = new SpriteSheet("assets/spritesheet.penguin.png", 16, 16);
        player.setSpritesheet(playerSheet);

        Animation standAnimation = new Animation();
        int standDuration = 1;
        standAnimation.addFrame(player.getSpritesheet().getSprite(0, 0), standDuration);
        standAnimation.setLooping(true);
        player.addAnimation("stand", standAnimation);

        Animation walkAnimation = new Animation();
        int walkDuration = 10;
        walkAnimation.addFrame(player.getSpritesheet().getSprite(0, 1), walkDuration);
        walkAnimation.addFrame(player.getSpritesheet().getSprite(1, 1), walkDuration);
        walkAnimation.addFrame(player.getSpritesheet().getSprite(2, 1), walkDuration);
        walkAnimation.addFrame(player.getSpritesheet().getSprite(3, 1), walkDuration);
        walkAnimation.setLooping(true);
        player.addAnimation("walk", walkAnimation);

        Animation flapAnimation = new Animation();
        int flapDuration = 10;
        flapAnimation.addFrame(player.getSpritesheet().getSprite(0, 2), flapDuration);
        flapAnimation.addFrame(player.getSpritesheet().getSprite(1, 2), flapDuration);
        flapAnimation.addFrame(player.getSpritesheet().getSprite(2, 2), flapDuration);
        flapAnimation.addFrame(player.getSpritesheet().getSprite(3, 2), flapDuration);
        flapAnimation.setLooping(true);
        player.addAnimation("flap", flapAnimation);

    }

    @Override
    public void update(GameContainer gc, int dt) throws SlickException {
        long time = System.nanoTime() * 1000;
        appgc.setTitle(TITLE + " @ " + appgc.getFPS() + " fps");

        for(int x = 0; x < gc.getWidth(); x++){
            if(Chance.integer(0,500) == 5){
                Snowflake flake = new Snowflake(x);
                snowflakes.add(flake);
            }
        }

        for(Iterator f = snowflakes.iterator(); f.hasNext();){
            Snowflake flake = (Snowflake) f.next();
            if(flake.getY() > gc.getHeight()){
                f.remove();
                continue;
            }else{
                flake.setY(flake.getY()+flake.getFallSpeed());
            }
        }

        if(gc.getInput().isKeyDown(Input.KEY_A)){
            player.move("left", dt);
            player.getAnimation("walk").update(dt);
        }else if(gc.getInput().isKeyDown(Input.KEY_D)){
            player.move("right", dt);
            player.getAnimation("walk").update(dt);
        }else{
            player.move("none", dt);
            //player.getAnimation("stand").update(dt);
        }

        player.getAnimation("flap").update(dt);

        for(Tile tile : world.getTiles()){
            Tile northTile = world.getTile(tile.getRow()-1, tile.getCol());
            Tile southTile = world.getTile(tile.getRow()+1, tile.getCol());
            Tile eastTile = world.getTile(tile.getRow(), tile.getCol()+1);
            Tile westTile = world.getTile(tile.getRow(), tile.getCol()-1);

            boolean matchNorth = false;
            boolean matchSouth = false;
            boolean matchEast = false;
            boolean matchWest = false;

            // Check for matches
            if(northTile != null && northTile.getType() == tile.getType()){
                matchNorth = true;
            }
            if(southTile != null && southTile.getType() == tile.getType()){
                matchSouth = true;
            }
            if(eastTile != null && eastTile.getType() == tile.getType()){
                matchEast = true;
            }
            if(westTile != null && westTile.getType() == tile.getType()){
                matchWest = true;
            }


        }

        int playerX = player.getX();
        int playerY = player.getY();
        int playerWidth = player.getSpritesheet().getSprite(0, 0).getWidth();
        int playerHeight = player.getSpritesheet().getSprite(0, 0).getHeight();

        int playerMoveX = 0;
        int playerMoveY = 1;

        player.setGrounded(false);

        for(Tile tile : world.getTiles()){
            int tileX = tile.getCol() * world.getTileSize();
            int tileY = tile.getRow() * world.getTileSize();
            int tileWidth = world.getTileSize();
            int tileHeight = world.getTileSize();

            if(playerX+playerWidth >= tileX && playerX <= tileX+tileWidth){
                // Player is above or below this tile
                if(playerY+playerHeight+playerMoveY > tileY && playerY+playerHeight <= tileY){
                    playerMoveY = 0;
                    player.setGrounded(true);
                }

            }
        }

        player.setY(player.getY()+playerMoveY);

    }

    @Override
    public void keyPressed(int key, char c) {
        super.keyPressed(key, c);
        switch(key){
            case Input.KEY_ESCAPE:
                appgc.exit();
                break;
            case Input.KEY_SPACE:
                player.jump();
                break;
        }
    }

    @Override
    public void render(GameContainer gc, Graphics g) throws SlickException {
        for(int x = 0; x < gc.getWidth(); x += world.getBackground().getWidth()){
            for(int y = 0; y < gc.getHeight(); y += world.getBackground().getHeight()){
                g.drawImage(world.getBackground(), x, y);
            }
        }

        g.setAntiAlias(true);
        float scale = 3f;
        g.scale(scale, scale);



        for(Tile tile : world.getTiles()){
            int x = tile.getCol() * world.getTileSize();
            int y = tile.getRow() * world.getTileSize();
            //double scale = (double) world.getTileSize() / (double) tile.getImage().getWidth();

            tile.getImage().setFilter(Image.FILTER_NEAREST);
            tile.getImage().draw(x, y);

            for(TileDecoration td : tile.getDecorations()){
                td.getImage().setFilter(Image.FILTER_NEAREST);
                td.getImage().draw(x + td.getOffsetX(), y + td.getOffsetY());
            }

        }

        // Draw the player
        int playerX = player.getX();
        int playerY = player.getY();
        //double playerScale = (double) world.getTileSize() / (double) player.getSpritesheet().getSprite(0, 0).getWidth();
        Image playerImage;
        if(player.isMovingLeft()){
            playerImage = player.getAnimation("walk").getCurrentFrame();
        }else if(player.isMovingRight()){
            playerImage = player.getAnimation("walk").getCurrentFrame().getFlippedCopy(true, false);
        }else if(!player.isGrounded()){
            playerImage = player.getAnimation("flap").getCurrentFrame();
        }else{
            playerImage = player.getAnimation("stand").getCurrentFrame();
        }

        playerImage.draw(playerX, playerY);

        // Draw the snowflakes
        for(Snowflake flake : snowflakes){
            g.setColor(Color.white);
            g.fillOval(flake.getX(), flake.getY(), flake.getSize(), flake.getSize());
        }
    }

    public Main(String title) {
        super(title);
        try {
            appgc = new AppGameContainer(this, 800, 600, false);
            appgc.setVSync(true);
            appgc.setShowFPS(false);
            appgc.start();
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args){
        new Main(TITLE);
    }
}
