import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/**
 * Created by fubar on 12/11/13.
 */
public class TileDecoration {
    private int offsetX;
    private int offsetY;
    private Image image;
    public TileDecoration(int offX, int offY, String img){
        offsetX = offX;
        offsetY = offY;
        try{
            image = new Image(img);
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }

    public int getOffsetX() {
        return offsetX;
    }

    public int getOffsetY() {
        return offsetY;
    }

    public Image getImage() {
        return image;
    }
}
