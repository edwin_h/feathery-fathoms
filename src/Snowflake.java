/**
 * Created by Fubar on 12/12/13.
 */
public class Snowflake {
    private int x;
    private int y;
    private int size;
    private int fallSpeed;
    public Snowflake(int initX){
        x = initX;
        y = -8;
        fallSpeed = Chance.integer(1,3);
        size = Chance.integer(1,3);
    }

    public int getFallSpeed() {
        return fallSpeed;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getSize() {
        return size;
    }
}
