import org.newdawn.slick.Animation;
import org.newdawn.slick.SpriteSheet;

import java.util.HashMap;

/**
 * Created by fubar on 12/11/13.
 */
public class Character {
    private String name;
    private SpriteSheet spritesheet;
    private HashMap<String, Animation> animations;
    private int spriteX;
    private int spriteY;
    private int x;
    private int y;
    private double moveSpeed = 0.07;
    private boolean isMovingLeft = false;
    private boolean isMovingRight = false;
    private boolean isGrounded = false;

    private double jumpSize = 24.0;

    public Character(String n){
        name = n;
        x = 0;
        y = 0;
        spriteX = 0;
        spriteY = 0;
        animations = new HashMap<>();
    }

    public void move(String d, int dt){
        double speed = Math.floor(moveSpeed * (double) dt);
        //System.out.println(speed);
        switch(d){
            case "left":
                x -= speed;
                isMovingLeft = true;
                isMovingRight = false;
                break;
            case "right":
                x += speed;
                isMovingLeft = false;
                isMovingRight = true;
                break;
            default:
                isMovingLeft = false;
                isMovingRight = false;
        }
    }

    public void jump(){
        if(isGrounded){
            y -= jumpSize;
        }

    }

    public void addAnimation(String n, Animation a){
        animations.put(n, a);
    }

    public Animation getAnimation(String n){
        return animations.get(n);
    }

    public String getName() {
        return name;
    }

    public SpriteSheet getSpritesheet() {
        return spritesheet;
    }

    public void setSpritesheet(SpriteSheet spritesheet) {
        this.spritesheet = spritesheet;
    }

    public int getSpriteX() {
        return spriteX;
    }

    public void setSpriteX(int spriteX) {
        this.spriteX = spriteX;
    }

    public int getSpriteY() {
        return spriteY;
    }

    public void setSpriteY(int spriteY) {
        this.spriteY = spriteY;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public double getMoveSpeed() {
        return moveSpeed;
    }

    public boolean isMovingLeft() {
        return isMovingLeft;
    }

    public boolean isMovingRight() {
        return isMovingRight;
    }

    public boolean isGrounded() {
        return isGrounded;
    }

    public void setGrounded(boolean isGrounded) {
        this.isGrounded = isGrounded;
    }

    public HashMap<String, Animation> getAnimations() {
        return animations;
    }
}
