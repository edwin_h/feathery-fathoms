import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import java.util.ArrayList;

/**
 * Created by fubar on 12/11/13.
 */
public class Tile {
    private int row;
    private int col;
    private String type;
    private Image image;
    private ArrayList<TileDecoration> decorations;
    public Tile(int r, int c) {
        decorations = new ArrayList<>();
        row = r;
        col = c;
    }
    public void checkType(){
        String newImage = "assets/tile.placeholder.png";
        if(type.equals("ice")){
            newImage = "assets/tile.icesnow.1.png";
            addTileDecoration(new TileDecoration(0, -6, "assets/decor.snow.png"));
        }

        try{
            image = new Image(newImage);
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }
    public String getType() {
        return type;
    }
    public void addTileDecoration(TileDecoration td){
        decorations.add(td);
    }

    public ArrayList<TileDecoration> getDecorations() {
        return decorations;
    }

    public void setType(String type) {
        this.type = type;
        checkType();
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public Image getImage() {
        return image;
    }
}
